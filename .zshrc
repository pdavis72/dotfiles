source ~/projects/antigen/antigen.zsh

# Load the oh-my-zsh library
 antigen use oh-my-zsh

# # Bundles from the deffault repo (robbyrussell's oh-my-zsh)
 antigen bundle git
 antigen bundle docker
 antigen bundle fedora
 antigen bundle gem
 antigen bundle go
 antigen bundle node
 antigen bundle npm
 antigen bundle perl
 antigen bundle python
 antigen bundle ruby
 antigen bundle ssh-agent
 antigen bundle sudo
 antigen bundle tmux
 antigen bundle vagrant
 antigen bundle vundle
 antigen bundle kennethreitz/autoenv
#
#
# # Syntax highlighting bundle.
 antigen bundle zsh-users/zsh-syntax-highlighting
#
# # Load the theme.
 antigen theme wezm+
#
# # Tell antigen that you're done.
 antigen apply
#
# Aliases
 alias p='tail -1 /home/pdavis/notes.txt | pbcopy'
 alias pbcopy='xsel --clipboard --input'
 alias console='cu -t -l /dev/ttyUSB0 -s 115200'
 alias home='ssh netinstall@pebcac.org'
 alias jumphost='ssh pdavis@jumphost.pebcac.org'
 alias rtrain='cd /home/pdavis/Dropbox/RedHat/Training'
 alias vimupdateplugins='vim +BunleInstall! +BundleClean! +q'
#
. $HOME/.shellrc.load
